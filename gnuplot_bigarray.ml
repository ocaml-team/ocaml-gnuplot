(* File: gnuplot_bigarray.ml

   Copyright (C) 2001-2004

     Christophe Troestler
     email: Christophe.Troestler@umh.ac.be
     WWW: http://math.umh.ac.be/an/software/

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public License
   version 2.1 as published by the Free Software Foundation, with the
   special exception on linking described in file LICENSE.

   This library is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the file
   LICENSE for more details.
*)
(* 	$Id: gnuplot_bigarray.ml,v 1.4 2007-11-27 23:07:22 chris_77 Exp $	 *)

open Bigarray
include Gnuplot_common_

(***********************************************************************
 *
 *                         BIGARRAY INTERFACE
 *
 ***********************************************************************)

type 'a vec = (float, float64_elt, 'a) Array1.t
type 'a mat = (float, float64_elt, 'a) Array2.t

(* 2D Plots *)

let is_c_layout v =
  Array1.layout v = (Obj.magic c_layout : 'a Bigarray.layout)

let x g ?tag ?style ?label ?n0 ?ofsx ?incx (xvec:'a vec) =
  if is_c_layout xvec
  then Gnuplot_ba_c.x g ?tag ?style ?label ?n0 ?ofsx ?incx (Obj.magic xvec)
  else Gnuplot_ba_f.x g ?tag ?style ?label ?n0 ?ofsx ?incx (Obj.magic xvec)

let xy g ?tag ?style ?label ?ofsx ?incx (xvec:'a vec)
  ?ofsy ?incy (yvec:'a vec) =
  if is_c_layout xvec
  then Gnuplot_ba_c.xy g ?tag ?style ?label
    ?ofsx ?incx (Obj.magic xvec) ?ofsy ?incy (Obj.magic yvec)
  else Gnuplot_ba_f.xy g ?tag ?style ?label
    ?ofsx ?incx (Obj.magic xvec) ?ofsy ?incy (Obj.magic yvec)

let bin g ?tag ?label ?center ?ofsx ?incx (xvec:'a vec)
  ?ofsy ?incy (yvec:'a vec) =
  if is_c_layout xvec
  then Gnuplot_ba_c.bin g ?tag ?label ?center
    ?ofsx ?incx (Obj.magic xvec) ?ofsy ?incy (Obj.magic yvec)
  else Gnuplot_ba_f.bin g ?tag ?label ?center
    ?ofsx ?incx (Obj.magic xvec) ?ofsy ?incy (Obj.magic yvec)

let vector g ?tag ?label ?ofsx ?incx (xvec:'a vec)
  ?ofsy ?incy (yvec:'a vec)
  ?ofsdx ?incdx (dxvec:'a vec) ?ofsdy ?incdy (dyvec:'a vec) =
  if is_c_layout xvec
  then Gnuplot_ba_c.vector g ?tag ?label
    ?ofsx ?incx (Obj.magic xvec) ?ofsy ?incy (Obj.magic yvec)
    ?ofsdx ?incdx (Obj.magic dxvec) ?ofsdy ?incdy (Obj.magic dyvec)
  else Gnuplot_ba_f.vector g ?tag ?label
    ?ofsx ?incx (Obj.magic xvec) ?ofsy ?incy (Obj.magic yvec)
    ?ofsdx ?incdx (Obj.magic dxvec) ?ofsdy ?incdy (Obj.magic dyvec)

let err g ?tag ?(xerr:'a vec option) (xvec:'a vec)
  ?(yerr:'a vec option) (yvec:'a vec) =
  if is_c_layout xvec
  then Gnuplot_ba_c.err g ?tag ?xerr:(Obj.magic xerr) (Obj.magic xvec)
    ?yerr:(Obj.magic yerr) (Obj.magic yvec)
  else Gnuplot_ba_f.err g ?tag ?xerr:(Obj.magic xerr) (Obj.magic xvec)
    ?yerr:(Obj.magic yerr) (Obj.magic yvec)

(* 3D Plots *)

let xyz g ?tag ?style ?label (xvec:'a vec) (yvec:'a vec) (zmat:'a mat) =
  if is_c_layout xvec
  then Gnuplot_ba_c.xyz g ?tag ?style ?label
    (Obj.magic xvec) (Obj.magic yvec) (Obj.magic zmat)
  else Gnuplot_ba_f.xyz g ?tag ?style ?label
    (Obj.magic xvec) (Obj.magic yvec) (Obj.magic zmat)
