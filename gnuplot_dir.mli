(* File: gnuplot_dir.mli

   Copyright (C) 2001-2004

     Christophe Troestler
     email: Christophe.Troestler@umh.ac.be
     WWW: http://math.umh.ac.be/an/software/

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public License
   version 2.1 as published by the Free Software Foundation, with the
   special exception on linking described in file LICENSE.

   This library is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the file
   LICENSE for more details.
*)
(* 	$Id: gnuplot_dir.mli,v 1.3 2004-11-28 23:49:39 chris_77 Exp $	 *)

(** Temporary dir creation and removal *)


val mk_temp_dir : string -> string -> string -> string
  (** [mk_temp_dir tmpdir prefix suffix] creates a temporary directory
    in [tmpdir] with name consisting of [prefix] concatenated with a
    suitably chosen number then [suffix]. *)

val remove_rec_dir : string -> unit
  (** [remove_rec_dir dirname] remove recursively the directory
  [dirname].  This function silenty fails (even if it cannot remove
  the directory, no exception is raised). *)
