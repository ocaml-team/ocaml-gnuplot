(* 	$Id: ex10.ml,v 1.3 2004-11-22 19:54:25 chris_77 Exp $	 *)
(* 3D hidden trials *)

module P = Gnuplot
open Parse_args

let is_finite x =
  match classify_float x with
  | FP_infinite | FP_nan -> false
  | _ -> true

let () =
  let sphere x y = sqrt(1. -. (x*.x +. y*.y))
  and sphere0 x y =
    let z = sqrt(1. -. (x*.x +. y*.y)) in
    if is_finite z then z else 0. in
  let g = P.init ?offline:(offline 1) (device 1) in
  P.env3 g (-1.5) 1.5 (-1.5) 1.5 0. 1.5;
  P.pen g 1;
  P.fxy g sphere0 (-1.5) 1.5 (-1.5) 1.5;
  P.pen g 3;
  P.xyz_ft g (fun t -> (cos t, sin t, 0.)) 0. 7.;
  P.close g
