(* 	$Id: ex3.ml,v 1.2 2004-11-22 19:54:26 chris_77 Exp $	 *)

module P = Gnuplot
open Parse_args

let sleep t = ignore(Unix.select [] [] [] t)

let () =
  let device = device 1 in
  let g = P.init ~nxsub:2 ~nysub:2 device ?offline:(offline 1) in
  P.pen_width g 1.;
  P.box g;
  P.pen g 1;
  P.fx g ~style:P.Impulses (fun x -> x) 0. 1.;
  P.pen g 2;
  P.fx g ~style:P.Linespoints (fun x -> x**2.) 0. 1.;

  P.adv g;
  P.pen g 0;
  P.box g;
  P.pen g 3;
  P.pen_width g 2.;
  P.title g "Spiral";
  P.xy_param g (fun t -> let r = 0.1 *. t in
                (r *. cos t, r *. sin t)) 0. 13.;

  P.adv g;
  P.pen g 0;
  P.env g 0. 10. ~ylog:true ~ygrid:true 1e-5 10.;
  P.pen g 1;
  P.fx g (fun x -> exp(-. x)) 0.01 10.;

  P.adv g;
  P.pen g 0;
  P.env g ~xgrid:true 0. 1. ~ygrid:true 0. 1.;
  P.pen g (-1); (* border color *)
  P.show ~tag:1 g; (* Set tag 1 to be shown -- by default new tags are
                      not displayed; a [show] command must be issued. *)
  List.iter (fun e ->
               P.fx ~tag:1 g (fun x -> x**e) 0. 1.;
               if device = P.X then sleep 0.5;
            ) [0.1; 0.25; 0.5; 1.; 2.; 4.; 10.];
  sleep 1.;
  (* Hide them all *)
  P.hide ~tag:1 ~immediately:true g;
  sleep 2.;
  (* Show them all again *)
  P.show ~tag:1 g;

  P.close g
