(* 	$Id: ex8.ml,v 1.2 2004-11-22 19:54:26 chris_77 Exp $	 *)
(* Functor and text demonstration *)

open Parse_args

(* Data structure *)
module Data =
struct
  type vec = (float * string) list
  let iter f vec = List.iter (fun (x,_) -> f x) vec

  type vec2 = (float * float * string) list
  let iter2 f vec2 = List.iter (fun (x,y,_) -> f x y) vec2

  type vec4 = unit
  type mat = unit
  let iter4 _ = failwith "Data.iter4"
  let iter_mat _ = failwith "Data.iter_mat"
end

module P = Gnuplot.Make(Data)

let labelled_xy = [
  (1., 4., "hello");
  (2., 0., "Bonjour");
]

let () =
  let g = P.init ?offline:(offline 1) (device 1) in
  P.box g;
  P.pen g 1;
  P.point g 3;
  P.point_width g 2.;
  List.iter (fun (x,y,t) ->
               P.text g x y t
            ) labelled_xy;
  P.xy g labelled_xy ~style:P.Points;
  P.close g
