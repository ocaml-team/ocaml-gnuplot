(* 	$Id: ex9.ml,v 1.3 2004-11-22 19:54:26 chris_77 Exp $	 *)
(* Font and color demonstration *)

module P = Gnuplot
open Printf
open Parse_args

let () =
  let g = P.init ?offline:(offline 1) (device 1) in
  P.pen g 3;
(*   P.font g "Helvetica"; *)
  P.env g (-1.) 1. (-1.) 1.; (* FIXME: different colors for xy *)
  P.show g;
  for i = 1 to 20 do
    P.font_size g i;
    let p = i mod 6 in
    P.pen g p;
    P.text g 0. (float i) (sprintf "Size %i, pen %i" i p)
  done;
  P.close g
