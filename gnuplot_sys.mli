(* File: gnuplot_unix.mli

   Copyright (C) 2001-2004

     Christophe Troestler
     email: Christophe.Troestler@umh.ac.be
     WWW: http://math.umh.ac.be/an/software/

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public License
   version 2.1 as published by the Free Software Foundation, with the
   special exception on linking described in file LICENSE.

   This library is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the file
   LICENSE for more details.
*)
(* 	$Id: gnuplot_sys.mli,v 1.4 2007-11-27 23:07:22 chris_77 Exp $	 *)

type t
  (** Abstract type to hold additional information for the gnuplot
    process handler.  This information is specific to an instance of
    gnuplot. *)

val open_gnuplot_out : bool -> ?pos:int * int -> float -> float ->
  bool -> out_channel * t
  (** [open_gnuplot_out persist ?xpos ?ypos xsize ysize color] opens a
    gnuplot process.  The arguments [xsize], [ysize], and [color] are
    only relevant for "X" terminals. *)

val open_file_out : string -> out_channel * t
  (** [open_file_out fname] opens the file [fname] for writing.
    Returns a channel to write and the necessary information to be
    able to close it with [close_out] -- so the return value is
    indistinguishable from the one of [open_gnuplot_out]. *)

val close_out : out_channel -> t -> unit
  (** [close_out g t] closes the gnuplot process and free the
    associated ressources.
    @raise Sys_error if a problem occurs when closing. *)

val set_terminal_X : out_channel -> float -> float -> bool -> unit
  (** [open_out g xsize ysize color] set the terminal according to the
    platform. *)

val open_temp_file : t -> string * out_channel
  (** [open_temp_file t] creates a temporary file and open it for
    writing according to the information in [t].  Returns its name and
    an output channel to it. *)
