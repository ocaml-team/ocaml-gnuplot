ocaml-gnuplot (0.8.3-6) unstable; urgency=medium

  * Team upload
  * Fix compilation with OCaml 5.2.0 (Closes: #1073895)

 -- Stéphane Glondu <glondu@debian.org>  Fri, 09 Aug 2024 08:18:31 +0200

ocaml-gnuplot (0.8.3-5) unstable; urgency=medium

  [ Stéphane Glondu ]
  * Team upload
  * Bump debhelper compat level to 13
  * Bump Standards-Version to 4.6.2
  * Add Rules-Requires-Root: no

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Use secure copyright file specification URI.
  * Set debhelper-compat version in Build-Depends.
  * Change priority extra to priority optional.
  * Set upstream metadata fields: Archive, Repository.

 -- Stéphane Glondu <glondu@debian.org>  Sat, 12 Aug 2023 11:07:48 +0200

ocaml-gnuplot (0.8.3-4) unstable; urgency=medium

  * Team upload
  * Update Vcs-*
  * Fix compilation with OCaml 4.08.0

 -- Stéphane Glondu <glondu@debian.org>  Wed, 04 Sep 2019 14:17:56 +0200

ocaml-gnuplot (0.8.3-3) unstable; urgency=low

  * Patch install-byte: "make install" must depend only on the files that are
    to be installed. Fixes FTBFS on archs without native ocaml compilation.
    Thanks to Hendrik Tews for the hint (closes: #663172).
  * Standards-version 3.9.3
    - debian/copyright: refer to official 1.0 format

 -- Ralf Treinen <treinen@debian.org>  Sat, 10 Mar 2012 14:38:02 +0100

ocaml-gnuplot (0.8.3-2) unstable; urgency=low

  * Fix licence in debian/copyright (GPL -> LGPL)

 -- Ralf Treinen <treinen@debian.org>  Wed, 08 Feb 2012 08:32:53 +0100

ocaml-gnuplot (0.8.3-1) unstable; urgency=low

  * Initial release (closes: #494747)

 -- Ralf Treinen <treinen@debian.org>  Mon, 06 Feb 2012 18:38:28 +0100
